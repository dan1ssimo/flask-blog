FROM python:3.8-slim-buster
COPY . /app

RUN apt update
RUN pip install -U pip
RUN pip install -U wheel
RUN pip install -U setuptools
ENV PYTHONPATH "${PYTHONPATH}:pwd"

WORKDIR /app
RUN pip install -r requirements.txt

# ENTRYPOINT [ "python" ]
# CMD [ "app.py" ]